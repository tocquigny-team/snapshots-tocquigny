#!/usr/bin/env node
var htmlSnapshots = require('html-snapshots');
var result = htmlSnapshots.run({
	input: "sitemap",
  source: "http://www.tocquigny.com/sitemap.xml",
  hostname : 'tocquigny.com',
  outputDir: "./snapshots",
  outputDirClean: true,
  selector: ".page",
  processLimit: 1,
  timeout: 30000,
  checkInterval : 500,
  pollInterval : 1000
}, function(err, results) {
  console.log("result = "+result);
  console.log("err = "+err);
});

console.log("started");